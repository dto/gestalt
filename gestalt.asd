(asdf:defsystem #:gestalt
  :depends-on (:xelf :zpng)
  :components ((:file "package")
               (:file "gestalt" :depends-on ("package"))))
