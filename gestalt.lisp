;;; gestalt.el --- procedural painting               -*- lexical-binding: t; -*-

;; Copyright (C) 2021  David O'Toole

;; Author: David O'Toole <dto@nomad>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(in-package :gestalt)

(defclass canvas (buffer)
  ((quadtree-depth :initform 10)
   (z-sort-p :initform nil)
   (background-color :initform "white")))

(defvar *zoom* nil)

(defmethod toggle-zoom ((canvas canvas))
  (setf *zoom* (if *zoom* nil t)))

(defmethod draw :around ((canvas canvas))
  (if *zoom*
      (let ((*gl-screen-height* (* *gl-screen-height* 8))
            (*gl-screen-width* (* *gl-screen-width* 8)))
        (call-next-method))
      (call-next-method)))

(defmethod initialize-instance :after ((canvas canvas) &key)
  (bind-event canvas '(:z :control) 'toggle-zoom)
  (resize canvas 1500 1500))

(defclass sphere (node)
  ((radius :initform 0.5 :initarg :radius :accessor radius)
   (color :initform "black")))

(defmethod initialize-instance :after ((sphere sphere) &key)
  (resize sphere (* 2 (radius sphere)) (* 2 (radius sphere))))

(defclass knife (node)
  ((radius :initform 10.0 :initarg :radius :accessor radius)
   (color :initform "black")
   (rate :initform 3000)
   (num-spheres :initform nil :initarg :num-spheres :accessor num-spheres)))

(defmethod initialize-instance :after ((knife knife) &key)
  (resize knife 100 100))

(defmethod paint ((sphere sphere) color)
  (setf (color sphere) color))

(defmethod paint ((knife knife) color)
  (setf (color knife) color))

(defmethod refill ((knife knife) &optional (num-spheres 10000) color)
  (when color (paint knife color))
  (setf (num-spheres knife) num-spheres))

(defmethod clean ((knife knife))
  (setf (num-spheres knife) 0))

(defmethod collide :around ((a sphere) (b sphere))
  (when (< (distance-between a b)
           (+ (radius a)
              (radius b)))
    (call-next-method)))

(defmethod collide ((moving sphere) (resting sphere))
  (move resting
        (heading-between moving resting)
        (radius moving)))
        
(defmethod draw ((sphere sphere))
  (with-slots (x y radius color) sphere
    (draw-textured-rectangle-* (- x radius)
                               (- y radius)
                               0
                               (* 2 radius)
                               (* 2 radius)
                               (find-texture "sphere.png")
                               :vertex-color color)))

(defmethod draw ((knife knife)) nil)

(defmethod drag ((knife knife) heading distance)
  (with-slots (radius color num-spheres rate) knife
    (block dragging
      (let ((step (/ distance rate)))
        (dotimes (n rate)
          (if (zerop num-spheres)
              (return-from dragging)
              (let ((sphere (make-instance 'sphere :color color)))
                (multiple-value-bind (x y) (center-point knife)
                  (add-node (current-buffer) sphere
                            (+ x (- (random (* radius 2)) radius))
                            (+ y (- (random (* radius 2)) radius)))
                  (xelf:quadtree-collide sphere (quadtree (current-buffer)))
                  (move knife heading step)
                  (setf (heading knife) heading)
                  (xelf:quadtree-collide knife (quadtree (current-buffer)))))))))))

(defmethod collide ((knife knife) (sphere sphere))
  (unless (string= (color knife)
                   (color sphere))
    (percent-of-time 20 (move sphere (heading knife) 1))))

(defresource "sphere.png")

(defmethod update :around ((canvas canvas)) nil)

(defmethod find-rgba ((node node))
  (mapcar #'(lambda (x)
              (truncate (* x 255)))
          (append (xelf::gl-color-values-from-string (color node))
                  (list 1))))

(defmethod render-png ((canvas canvas))
  (let ((png (make-instance 'zpng:pixel-streamed-png
                            :color-type :truecolor-alpha
                            :width (width canvas)
                            :height (height canvas))))
    (with-open-file (stream "test.png"
                            :direction :output
                            :if-exists :supersede
                            :if-does-not-exist :create
                            :element-type '(unsigned-byte 8))
      (zpng:start-png png stream)
    (dotimes (x (width canvas))
      (dotimes (y (height canvas))
        (let (found)
          (quadtree-map-collisions (quadtree (current-buffer))
                                   (cfloat y)
                                   (cfloat x)
                                   (cfloat (+ x 1))
                                   (cfloat (+ y 1))
                                   #'(lambda (thing)
                                       (setf found thing)))
          (if found
              (zpng:write-pixel (find-rgba found) png)
              (zpng:write-pixel (list 255 255 255 255) png)))))
      (zpng:finish-png png))))

(defun gestalt ()
  (with-session
    (open-project "gestalt")
    (setf *frame-rate* 60)
    (setf *screen-width* 1280)
    (setf *screen-height* 720)
    (setf *nominal-screen-width* *screen-width*)
    (setf *nominal-screen-height* *screen-height*)
    (setf *scale-output-to-window* nil)
    (index-pending-resources)
    (preload-resources)
    (switch-to-buffer (make-instance 'canvas))
    (let ((knife (make-instance 'knife)))
      (add-node (current-buffer) knife 100 100)
      (install-quadtree (current-buffer))
      (refill knife 10000 "red")
      (drag knife (direction-heading :right) 500)
      (refill knife 10000 "orange")
      (drag knife (direction-heading :down) 500)
      (clean knife)
      (refill knife 10000 "hot pink")
      (drag knife (direction-heading :upleft) 500)
      (refill knife 1000 "tomato")
      (drag knife (direction-heading :right) 600)
      (refill knife 1000 "goldenrod")
      (drag knife (direction-heading :left) 800)
      (refill knife 1000 "olive drab")
      (drag knife (direction-heading :down) 800)
      (dotimes (n 20)
        (move-to knife (random 1000) (random 1000))
        (paint knife (random-choose '("salmon" "light salmon" "navajo white" "indian red" "aquamarine")))
        (drag knife (direction-heading :upright) 800))
      (dotimes (n 10)
        (move-to knife (random 1000) (random 1000))
        (paint knife (random-choose '("salmon" "light salmon" "navajo white" "indian red" "aquamarine")))
        (drag knife (direction-heading :right) 800)))))
      ;;(render-png (current-buffer)))))
      ;; (let ((knife2 (make-instance 'knife :radius 25)))
      ;;   (refill knife2 20000 "gray70")
      ;;   (dotimes (n 5)
      ;;     (move-to knife2 (random 1000) (random 1000))
      ;;     (drag knife2 (direction-heading (random-choose '(:up :down :left :right :upleft :upright :downleft :downright))) 300))))))
                   


;;; gestalt.lisp ends here
